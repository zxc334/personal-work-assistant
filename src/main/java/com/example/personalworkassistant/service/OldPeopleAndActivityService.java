package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.OldPeopleAndActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.OlderActivityInfo;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【old_people_and_activity】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface OldPeopleAndActivityService{

    /**
     * 根据老人ID查询活动具体信息
     * @param olderId
     * @return
     */
    List<OlderActivityInfo> queryActivityInfo(String olderId);
}
