package com.example.personalworkassistant.service.impl;

import com.example.personalworkassistant.domain.Photo;
import com.example.personalworkassistant.mapper.PhotoMapper;
import com.example.personalworkassistant.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private PhotoMapper photoMapper;

    @Override
    public List<Photo> queryPhotoAddress() {
        List<Photo> list = photoMapper.selectList(null);
        return list;
    }

    @Override
    public void insertPhoto(List<String> photoAddressArr) {
        photoMapper.deleteAll();
        for (String photoAddress : photoAddressArr) {
            Photo photo = new Photo();
            photo.setAddress(photoAddress);
            photoMapper.insert(photo);
        }
    }


    private static boolean isImageFile(File file) {
        String fileName = file.getName().toLowerCase();
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex == -1) {
            return false;
        }
        String fileExtension = fileName.substring(dotIndex + 1);
        return "jpg".equals(fileExtension) || "jpeg".equals(fileExtension) ||
                "png".equals(fileExtension) || "gif".equals(fileExtension);
    }

}
