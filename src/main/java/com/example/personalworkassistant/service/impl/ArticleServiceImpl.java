package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.domain.Article;
import com.example.personalworkassistant.mapper.ActivityMapper;
import com.example.personalworkassistant.mapper.CategoryMapper;
import com.example.personalworkassistant.service.ArticleService;
import com.example.personalworkassistant.mapper.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【article】的数据库操作Service实现
* @createDate 2024-01-19 18:11:38
*/
@Service
public class ArticleServiceImpl implements ArticleService{
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public List<Article> queryByCategory(String categoryId) {
        LambdaQueryWrapper<Article> articleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleLambdaQueryWrapper.eq(Article::getCategoryId,categoryId);
        List<Article> articles = articleMapper.selectList(articleLambdaQueryWrapper);
        return articles;
    }

    @Override
    public Article addArticle(Article article) {
        Article article1 = new Article();
        article.setId(article1.getId());
        articleMapper.insert(article);
        return article;
    }

    @Override
    public List<Article> searchArticle(String info) {
        LambdaQueryWrapper<Article> articleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleLambdaQueryWrapper.like(Article::getTitle,info);
        articleLambdaQueryWrapper.orderByDesc(Article::getQuantity);
        List<Article> articles = articleMapper.selectList(articleLambdaQueryWrapper);
        return articles;
    }

    @Override
    public Article addQuantity(String articleId) {
        Article article = articleMapper.selectById(articleId);
        article.setQuantity(article.getQuantity()+1);
        articleMapper.updateById(article);
        Article article1 = articleMapper.selectById(articleId);
        return article1;
    }

}




