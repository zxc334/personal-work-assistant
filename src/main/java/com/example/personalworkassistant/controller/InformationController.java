package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Article;
import com.example.personalworkassistant.domain.Category;
import com.example.personalworkassistant.domain.User;
import com.example.personalworkassistant.dto.CreateActivity;
import com.example.personalworkassistant.dto.OlderBasicInfo;
import com.example.personalworkassistant.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lzf
 * @version 1.0
 */
@Api(value = "资讯模块相关接口")
@RestController
@RequestMapping("/api/information")
@Slf4j
@CrossOrigin
public class InformationController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserService userService;

    @ApiOperation(value = "获取对应的资讯分类")
    @GetMapping("/queryAllCategory")
    public ResponseDTO queryAllCategory() {
        List<Category> categories = categoryService.queryAllCategory();
        return ResponseDTO.success("获取成功",categories);
//        User user = userService.queryUser(userId);
//        if (user!=null){
//            List<Category> categories = categoryService.queryAllCategory();
//            return ResponseDTO.success("获取成功", categories);
//        }
//        return ResponseDTO.fail("获取失败，请先登录", null);
    }

    @ApiOperation(value = "添加资讯分类")
    @PostMapping("/addCategory")
    public ResponseDTO addCategory(@RequestBody Category category) {
        Category category1 = categoryService.addCategory(category);
        return ResponseDTO.success("添加成功", category1);
    }

    @ApiOperation(value = "删除资讯分类")
    @DeleteMapping("/deleteCategory")
    public ResponseDTO deleteCategory(@RequestParam("categoryId") String categoryId) {
        categoryService.deleteCategory(categoryId);
        return ResponseDTO.success("删除成功", null);
    }

    @ApiOperation(value = "修改资讯分类")
    @PostMapping("/updateCategory")
    public ResponseDTO updateCategory(@RequestBody Category category) {
        categoryService.updateCategory(category);
        return ResponseDTO.success("修改成功", null);
    }

    @ApiOperation(value = "根据分类ID获取对应的推荐文章")
    @GetMapping("/queryArticalByCategory")
    public ResponseDTO queryArticalByCategory(@RequestParam("categoryId") String categoryId) {
        List<Article> articles = articleService.queryByCategory(categoryId);
        return ResponseDTO.success("获取成功", articles);
    }

}