package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName old_people_and_recommend
 */
@TableName(value ="old_people_and_recommend")
@Data
public class OldPeopleAndRecommend extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 老人ID
     */
    private String oldPeopleId;

    /**
     * 推荐ID
     */
    private String recommendId;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
