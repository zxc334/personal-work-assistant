package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName old_people
 */
@TableName(value ="old_people")
@Data
public class OldPeople extends CommonBase implements Serializable {
    /**
     * 老人ID
     */
    @TableId
    private String oldPeopleId;

    /**
     * 关系（父子、母子等）
     */
    private String relationship;

    /**
     * 用户ID（和用户表进行关联）
     */
    private String userId;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 体重
     */
    private Integer weight;

    /**
     * 身高
     */
    private Double height;

    /**
     * 爱好
     */
    private String hobby;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
