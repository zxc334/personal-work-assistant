package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.CreateActivity;
import com.example.personalworkassistant.dto.OlderBasicInfo;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【category】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface CategoryService{
    /**
     * 获取所有分类的信息
     */
    List<Category> queryAllCategory();

    /**
     * 添加分类
     * @param category
     */
    Category addCategory(Category category);

    /**
     * 删除分类
     * @param categoryId
     */
    void deleteCategory(String categoryId);

    /**
     * 修改分类
     * @param category
     */
    Category updateCategory(Category category);
}
