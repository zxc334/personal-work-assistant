package com.example.personalworkassistant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.personalworkassistant.domain.Feedback;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lzf
 * @version 1.0
 */
@Mapper
public interface FeedbackMapper extends BaseMapper<Feedback> {
}
