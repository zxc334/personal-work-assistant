package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.OlderBasicInfo;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【article】的数据库操作Service
* @createDate 2024-01-19 18:11:38
*/
public interface ArticleService{
    /**
     * 根据分类ID获取对应的文章
     * @param categoryId
     */
    List<Article> queryByCategory(String categoryId);

    /**
     * 上传文章
     * @param article
     */
    Article addArticle(Article article);

    /**
     * 搜索文章
     * @param info
     */
    List<Article> searchArticle(String info);

    /**
     * 修改文章
     * @param articleId
     */
    Article addQuantity(String articleId);
}
