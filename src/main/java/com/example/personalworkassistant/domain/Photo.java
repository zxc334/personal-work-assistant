package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 图片表
 */
@Data
public class Photo {
    /**
     * ID
     */
    @TableId
    private String id;
    /**
     * 图片的网络地址
     */
    private String address;
}
