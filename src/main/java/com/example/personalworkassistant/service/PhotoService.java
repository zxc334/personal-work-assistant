package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Photo;

import java.util.List;

public interface PhotoService {
    List<Photo> queryPhotoAddress();

    void insertPhoto(List<String> photoAddressArr);
}
