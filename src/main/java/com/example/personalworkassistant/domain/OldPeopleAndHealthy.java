package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName old_people_and_healthy
 */
@TableName(value ="old_people_and_healthy")
@Data
public class OldPeopleAndHealthy extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 老人ID
     */
    private String oldPeopleId;

    /**
     * 高压值
     */
    private Integer bloodPressureHigher;

    /**
     * 低压值
     */
    private Integer bloodPressureLower;


    /**
     * 血糖值
     */
    private Double bloodGlucose;

    /**
     * 饭后血糖值
     */
    private Double bloodGlucoseAftereat;


    /**
     * 心跳值
     */
    private Integer heartbeat;

    /**
     * 睡眠时间
     */
    private Integer sleepingTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
