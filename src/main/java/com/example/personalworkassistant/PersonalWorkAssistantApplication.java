package com.example.personalworkassistant;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})

@Slf4j
@EnableSwagger2
//@EnableTransactionManagement
@EnableCaching
@MapperScan("com.example.personalworkassistant.mapper")
@CrossOrigin
public class PersonalWorkAssistantApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalWorkAssistantApplication.class, args);
        log.info("启动成功啦(　o=^•ェ•)o　┏━┓");
    }

}
