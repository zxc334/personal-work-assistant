package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.OldPeople;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.OlderBasicInfo;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【old_people】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface OldPeopleService{

    /**
     * 根据用户ID获取对应的老人信息
     * @param userId
     */
    List<OlderBasicInfo> queryAllOlder(String userId);
}
