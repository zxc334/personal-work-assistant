package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.personalworkassistant.domain.Dialog;
import com.example.personalworkassistant.dto.DialogDto;
import com.example.personalworkassistant.mapper.DialogMapper;
import com.example.personalworkassistant.service.DialogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DialogServiceImpl implements DialogService {

    @Autowired
    private DialogMapper dialogMapper;

    /**
     * 向对话表添加一条记录
     *
     * @param dialogDto 对话消息
     */
    @Override
    public void createMsg(DialogDto dialogDto) {
        Dialog dialog = new Dialog();
        long time = System.currentTimeMillis();
        dialog.setTime(time);
        dialog.setUserContent(dialogDto.getUserContent());
        dialog.setBotContent(dialogDto.getBotContent());
        dialogMapper.insert(dialog);
        //防止读取不到最新的数据
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询对话信息
     */
    @Override
    public List<Dialog> queryMsg() {
        LambdaQueryWrapper<Dialog> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByAsc(Dialog::getTime);
        List<Dialog> dialogs = dialogMapper.selectList(wrapper);
        return dialogs;
    }
}
