package com.example.personalworkassistant.service.impl;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Feedback;
import com.example.personalworkassistant.mapper.FeedbackMapper;
import com.example.personalworkassistant.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lzf
 * @version 1.0
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackMapper feedbackMapper;

    @Override
    public ResponseDTO feedback(String feedback) {
        Feedback feedback1 = new Feedback();
        feedback1.setFeedback(feedback);
        feedbackMapper.insert(feedback1);
        return ResponseDTO.success("反馈已提交",feedback1);
    }
}
