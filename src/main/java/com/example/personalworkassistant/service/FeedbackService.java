package com.example.personalworkassistant.service;

import com.example.personalworkassistant.common.ResponseDTO;

/**
 * @author lzf
 * @version 1.0
 */
public interface FeedbackService {
    ResponseDTO feedback(String feedback);
}
