package com.example.personalworkassistant.dto;

import lombok.Data;

@Data
public class OlderActivityInfo {
    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 备注信息（如何进行运动，注意事项等）
     */
    private String remark;

    /**
     * 老人ID
     */
    private String oldPeopleId;

    /**
     * 活动时间
     */
    private String activeTime;

    /**
     * 今日锻炼时间（分钟）
     */
    private String todayExerciseTime;

    /**
     * 是否完成
     */
    private Integer isCompleted;
}
