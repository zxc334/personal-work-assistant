package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.example.personalworkassistant.dto.CreateActivity;
import com.example.personalworkassistant.dto.Task;
import com.example.personalworkassistant.dto.TaskDto;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【activity(定时任务调度表)】的数据库操作Service
* @createDate 2024-01-19 18:11:38
*/
public interface ActivityService{

    /**
     * 创建活动
     * @param createActivity
     */
    String createActivity(CreateActivity createActivity,String olderId);
    /**
     * 根据Id查询活动
     * @param id
     */
    Activity quaryActivity(String id);
    /**
     * 根据用户Id查询所有活动
     * @param childrenAndActivityList
     */
    List<Task> quaryAllActivity(List<ChildrenAndActivity> childrenAndActivityList);

    String createActivity(TaskDto task, String olderId);
}
