package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

import java.io.Serializable;

/**
 * 团建表
 */
@TableName(value ="team_activity")
@Data
public class TeamActivity implements Serializable {
    /**
     * ID
     */
    @TableId
    private String Id;

    /**
     * 名称
     */
    private String name;

    /**
     * 介绍
     */
    private String note;

    /**
     * 时间
     */
    private String time;

    /**
     * 地点
     */
    private String address;

    /**
     * 是否已结束
     */
    private int isAchieve;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
