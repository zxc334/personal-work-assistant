package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.OldPeopleAndHealthy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.OlderPhysicalCondition;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【old_people_and_healthy】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface OldPeopleAndHealthyService{

    /**
     * 根据老人ID获取身体情况信息
     * @param olderId
     * @return
     */
    List<OlderPhysicalCondition> queryOlderHealthy(String olderId);
}
