package com.example.personalworkassistant.service;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.OlderBasicInfo;
import com.example.personalworkassistant.dto.UserDto;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【user】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface UserService{
    /**
     * 根据用户ID获取对应的用户信息
     * @param userId
     */
    User queryUser(String userId);

    ResponseDTO register(UserDto dto);

    ResponseDTO login(UserDto dto);

    ResponseDTO feedback(String feedback);
}
