package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.User;
import com.example.personalworkassistant.dto.UserDto;
import com.example.personalworkassistant.mapper.ActivityMapper;
import com.example.personalworkassistant.service.UserService;
import com.example.personalworkassistant.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Objects;

/**
* @author Lenovo
* @description 针对表【user】的数据库操作Service实现
* @createDate 2024-01-19 18:11:39
*/
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryUser(String userId) {
        User user = userMapper.selectById(userId);
        return user;
    }

    @Override
    public ResponseDTO register(UserDto dto) {
        // 校验手机号是否已注册
        String username = dto.getUsername();
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getUsername,username);
        if (userMapper.selectCount(userLambdaQueryWrapper)>0){
            // 手机号已注册
            return ResponseDTO.fail("该账号已经注册",null);
        }
        // 注册成功，保存用户信息
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setAge(dto.getAge());
        user.setNickName(dto.getNickName());
        user.setSex(dto.getSex());
        user.setPassword(DigestUtils.md5DigestAsHex(dto.getPassword().getBytes(StandardCharsets.UTF_8)));
        user.setOpenid(user.getId());
        // 注册成功，保存用户信息
        userMapper.insert(user);
        return ResponseDTO.success("注册成功",null);
    }

    @Override
    public ResponseDTO login(UserDto dto) {
        // 查询用户信息
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getUsername,dto.getUsername());
        User user = userMapper.selectOne(userLambdaQueryWrapper);
        if (user==null){
            return ResponseDTO.fail("用户不存在",null);
        }
        if (!Objects.equals(user.getPassword()
                , DigestUtils.md5DigestAsHex(dto.getPassword().getBytes(StandardCharsets.UTF_8)))) {
            // 密码错误
            return ResponseDTO.fail("用户名或密码错误",null);
        }
        return ResponseDTO.success("登录成功",user);
    }

    @Override
    public ResponseDTO feedback(String feedback) {

        return null;
    }
}




