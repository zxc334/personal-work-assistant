package com.example.personalworkassistant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.personalworkassistant.domain.TeamActivity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeamActivityMapper extends BaseMapper<TeamActivity> {
}
