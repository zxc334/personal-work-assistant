package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.Category;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.example.personalworkassistant.dto.*;
import com.example.personalworkassistant.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lzf
 * @version 1.0
 */
@Api(value = "亲子模块相关接口")
@RestController
@RequestMapping("/api/children")
@Slf4j
@CrossOrigin
public class ChildrenController {
    @Autowired
    private ChildrenService childrenService;
    @Autowired
    private ChildrenAndActivityService childrenAndActivityService;
    @Autowired
    private ActivityService activityService;

    @ApiOperation(value = "根据小孩ID获取任务信息")
    @GetMapping("/queryChildrenTask")
    public ResponseDTO queryOlderHealthy(@RequestParam("childrenId") String childrenId) {
        List<ChildrenAndActivity> childrenAndActivityList = childrenAndActivityService.queryTask(childrenId);
        List<Task> tasks = activityService.quaryAllActivity(childrenAndActivityList);
        return ResponseDTO.success("获取成功", tasks);
    }

    @ApiOperation(value = "添加任务")
    @PostMapping("/createTask")
    public ResponseDTO createTask(@RequestBody TaskDto task) {
        String activityId = activityService.createActivity(task, null);
        ChildrenAndActivity childrenAndActivity = childrenAndActivityService.addTask(task.getId(), activityId);
        return ResponseDTO.success("添加成功", childrenAndActivity);
    }

    @ApiOperation(value = "根据id统计小孩近三周的作业完成情况")
    @GetMapping("/homeworkInfo")
    public ResponseDTO homeworkInfo(@RequestParam("childrenId") String childrenId) {
        //int[][] homework = childrenAndActivityService.queryHomework(childrenId);
        return ResponseDTO.success("获取成功", childrenAndActivityService.queryHomework(childrenId));
    }

    @ApiOperation(value = "根据id统计小孩本月的锻炼情况")
    @GetMapping("/exerciseInfo")
    public ResponseDTO exerciseInfo(@RequestParam("childrenId") String childrenId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.queryExercise(childrenId));
    }
    @ApiOperation(value = "根据id统计小孩本月的所有活动完成情况")
    @GetMapping("/allInfo")
    public ResponseDTO allInfo(@RequestParam("childrenId") String childrenId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.queryAll(childrenId));
    }

    @ApiOperation(value = "根据id查询展示八个小孩随机选择的活动")
    @GetMapping("/randomDisplay")
    public ResponseDTO randomDisplay(@RequestParam("childrenId") String childrenId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.randomDisplay(childrenId));
    }

    @ApiOperation(value = "随机1-8选择活动")
    @GetMapping("/randomNum")
    public ResponseDTO randomNum(@RequestParam("childrenId") String childrenId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.randomNum(childrenId));
    }

    @ApiOperation(value = "记录选择的活动")
    @GetMapping("/randomUpdate")
    public ResponseDTO randomUpdate(@RequestParam("activityId") String activityId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.randomUpdate(activityId));
    }

    @ApiOperation(value = "历史随机选活动以选中查询记录")
    @GetMapping("/randomRecord")
    public ResponseDTO randomRecord(@RequestParam("childrenId") String childrenId) {
        return ResponseDTO.success("获取成功", childrenAndActivityService.randomRecord(childrenId));
    }
//    @ApiOperation(value = "删除任务")
//    @DeleteMapping("/deleteTask")
//    public ResponseDTO deleteTask(@RequestParam("categoryId") String categoryId) {
//        categoryService.deleteCategory(categoryId);
//        return ResponseDTO.success("删除成功", null);
//    }
//
//    @ApiOperation(value = "修改任务")
//    @PostMapping("/updateTask")
//    public ResponseDTO updateTask(@RequestBody Category category) {
//        categoryService.updateCategory(category);
//        return ResponseDTO.success("修改成功", null);
//    }

}
