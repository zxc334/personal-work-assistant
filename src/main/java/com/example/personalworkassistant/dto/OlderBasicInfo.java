package com.example.personalworkassistant.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class OlderBasicInfo {
    /**
     * 老人ID
     */
    @TableId
    private String oldPeopleId;

    /**
     * 关系（父子、母子等）
     */
    private String relationship;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 体重
     */
    private Integer weight;

    /**
     * 身高
     */
    private Double height;

    /**
     * 爱好
     */
    private String hobby;
}
