package com.example.personalworkassistant.dto;

import lombok.Data;

@Data
public class TeamActivityDto {
    /**
     * 名称
     */
    private String name;

    /**
     * 介绍
     */
    private String note;

    /**
     * 时间
     */
    private String time;

    /**
     * 地点
     */
    private String address;

    /**
     * 是否已结束
     */
    private int isAchieve;
}
