package com.example.personalworkassistant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.personalworkassistant.domain.Activity;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Lenovo
* @description 针对表【activity(定时任务调度表)】的数据库操作Mapper
* @createDate 2024-01-19 18:11:38
* @Entity com.example.personalworkassistant.domain.Activity
*/
@Mapper
public interface ActivityMapper extends BaseMapper<Activity> {

}




