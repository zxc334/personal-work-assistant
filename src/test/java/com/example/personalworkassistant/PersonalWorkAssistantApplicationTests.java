package com.example.personalworkassistant;

import com.example.personalworkassistant.domain.OldPeopleAndHealthy;
import com.example.personalworkassistant.domain.User;
import com.example.personalworkassistant.mapper.OldPeopleAndHealthyMapper;
import com.example.personalworkassistant.mapper.OldPeopleMapper;
import com.example.personalworkassistant.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PersonalWorkAssistantApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OldPeopleMapper oldPeopleMapper;
    @Autowired
    private OldPeopleAndHealthyMapper oldPeopleAndHealthyMapper;

    /**
     * 新建用户
     */
    @Test
    void insertUser() {
        User user = new User();
        user.setUsername("阿龙");
        user.setPassword("123456");
        userMapper.insert(user);
    }

    /**
     * 新建老人健康信息
     */
    @Test
    void insertOlderHealthy() {
        OldPeopleAndHealthy oldPeopleAndHealthy = new OldPeopleAndHealthy();
        oldPeopleAndHealthy.setOldPeopleId("1748520542017544194");
        oldPeopleAndHealthy.setHeartbeat(63);
        oldPeopleAndHealthy.setBloodGlucose(6.6);
        oldPeopleAndHealthy.setBloodGlucoseAftereat(9.2);
        oldPeopleAndHealthy.setBloodPressureHigher(150);
        oldPeopleAndHealthy.setBloodPressureLower(91);
        oldPeopleAndHealthy.setSleepingTime(7);
        oldPeopleAndHealthyMapper.insert(oldPeopleAndHealthy);
    }

}
