package com.example.personalworkassistant.dto;

import lombok.Data;

import java.util.List;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class WeekInfo {
    private List<Info> series;
}
