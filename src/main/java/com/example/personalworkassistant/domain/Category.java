package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName category
 */
@TableName(value ="category")
@Data
public class Category extends CommonBase implements Serializable {
    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer sort;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
