package com.example.personalworkassistant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.personalworkassistant.domain.Dialog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DialogMapper extends BaseMapper<Dialog> {
}
