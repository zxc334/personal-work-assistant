package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.TeamActivity;
import com.example.personalworkassistant.dto.*;
import com.example.personalworkassistant.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Api(value = "老人模块相关接口")
@RestController
@RequestMapping("/api/older")
@Slf4j
@CrossOrigin
public class OlderController {

    @Autowired
    private OldPeopleService oldPeopleService;
    @Autowired
    private OldPeopleAndHealthyService oldPeopleAndHealthyService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private OldPeopleAndActivityService oldPeopleAndActivityService;
    @Autowired
    private TeamActivityService teamActivityService;

    @ApiOperation(value = "根据用户ID获取对应的老人信息")
    @GetMapping("/queryAllOlder")
    public ResponseDTO queryAllOlder(@RequestParam("userId") String userId) {
        List<OlderBasicInfo> list = oldPeopleService.queryAllOlder(userId);
        return ResponseDTO.success("获取成功", list);
    }

    @ApiOperation(value = "根据老人ID获取身体情况信息")
    @GetMapping("/queryOlderHealthy")
    public ResponseDTO queryOlderHealthy(@RequestParam("olderId") String olderId) {
        List<OlderPhysicalCondition> list = oldPeopleAndHealthyService.queryOlderHealthy(olderId);
        return ResponseDTO.success("获取成功", list);
    }

    @ApiOperation(value = "添加活动")
    @PostMapping("/createActivity")
    public ResponseDTO createActivity(@RequestBody CreateActivity createActivity, @RequestParam("olderId") String olderId) {
        activityService.createActivity(createActivity, olderId);
        return ResponseDTO.success("创建成功", null);
    }

    @ApiOperation(value = "根据老人ID查询活动具体信息")
    @GetMapping("/queryActivityInfo")
    public ResponseDTO queryActivityInfo(@RequestParam("olderId") String olderId) throws Exception {
        List<OlderActivityInfo> list = oldPeopleAndActivityService.queryActivityInfo(olderId);
        //获取天气
        URL url = new URL("http://t.weather.itboy.net/api/weather/city/101250601");
        InputStreamReader isReader = new InputStreamReader(url.openStream(), "UTF-8");//“UTF- 8”万国码，可以显示中文，这是为了防止乱码
        BufferedReader br = new BufferedReader(isReader);//采用缓冲式读入
        String weather = null;
        String str;
        while ((str = br.readLine()) != null) {
            String regex = "\\p{Punct}+";
            String digit[] = str.split(regex);
            weather += ("天气:" + digit[67] + " " + digit[63] + digit[65] + "  ");
            weather += ("温度:" + digit[47] + "~" + digit[45]);
        }
        br.close();
        isReader.close();
//        System.out.println("天气信息：" + weather.substring(4));
        //存放天气信息和活动安排的数组
        List result = new ArrayList<>();
        result.add(weather.substring(4));
        result.add(list);
        return ResponseDTO.success("获取成功", result);
    }

    @ApiOperation(value = "添加团建活动")
    @PostMapping("/createTeamActivity")
    public ResponseDTO createTeamActivity(@RequestBody TeamActivityDto teamActivityDto) {
        teamActivityService.createTeamActivity(teamActivityDto);
        return ResponseDTO.success("创建成功", null);
    }

    @ApiOperation(value = "查询团建活动")
    @GetMapping("/queryTeamActivity")
    public ResponseDTO queryTeamActivity() {
        List<TeamActivity> lists = teamActivityService.queryTeamActivity();
        return ResponseDTO.success("查询成功", lists);
    }
}
