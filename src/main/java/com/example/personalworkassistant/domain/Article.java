package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName article
 */
@TableName(value ="article")
@Data
public class Article extends CommonBase implements Serializable {
    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 分类id
     */
    private String categoryId;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 文章封面
     */
    private String avatar;

    /**
     * 文章简介
     */
    private String summary;

    /**
     * 文章内容 （最多两百字）
     */
    private String content;

    /**
     * 是否发布 0：下架 1：发布
     */
    private Integer isPublish;

    /**
     * 文章阅读量
     */
    private Integer quantity;

    /**
     * 是否首页轮播
     */
    private Integer isCarousel;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
