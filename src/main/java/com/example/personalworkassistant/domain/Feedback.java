package com.example.personalworkassistant.domain;

/**
 * @author lzf
 * @version 1.0
 */

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 *
 * @TableName feedback
 */
@TableName(value ="feedback")
@Data
public class Feedback {
    /**
     * ID
     */
    @TableId
    private String id;

    /**
     * 反馈内容
     */
    private String feedback;
}
