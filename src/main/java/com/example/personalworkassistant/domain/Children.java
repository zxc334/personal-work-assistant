package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName children
 */
@TableName(value ="children")
@Data
public class Children extends CommonBase implements Serializable {
    /**
     * 孩子ID
     */
    private String childrenId;

    /**
     * 关系（父子、母子、父女、母女、大儿子、小儿子）
     */
    private String relationship;

    /**
     * 用户ID（和用户表进行关联）
     */
    private String userId;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
