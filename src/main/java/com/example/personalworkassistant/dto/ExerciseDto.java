package com.example.personalworkassistant.dto;

import lombok.Data;

import java.util.Arrays;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class ExerciseDto {
    private String name;
    private int[] data=new int[7];
    public int getSumOfData() {
        return Arrays.stream(data).sum();
    }
}
