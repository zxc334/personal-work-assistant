package com.example.personalworkassistant.dto;

import lombok.Data;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class UserDto {
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 年龄
     */
    private String age;
    /**
     * 性别
     */
    private String sex;

    /**
     * 密码
     */
    private String password;
}
