package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.domain.OldPeople;
import com.example.personalworkassistant.dto.OlderBasicInfo;
import com.example.personalworkassistant.service.OldPeopleService;
import com.example.personalworkassistant.mapper.OldPeopleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lenovo
 * @description 针对表【old_people】的数据库操作Service实现
 * @createDate 2024-01-19 18:11:39
 */
@Service
public class OldPeopleServiceImpl implements OldPeopleService {

    @Autowired
    private OldPeopleMapper oldPeopleMapper;

    /**
     * 根据用户ID获取对应的老人信息
     *
     * @param userId
     */
    @Override
    public List<OlderBasicInfo> queryAllOlder(String userId) {
        LambdaQueryWrapper<OldPeople> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OldPeople::getUserId,userId);
        List<OldPeople> oldPeoples = oldPeopleMapper.selectList(wrapper);
        List<OlderBasicInfo> list = new ArrayList<>();
        for (OldPeople oldPeople : oldPeoples) {
            OlderBasicInfo olderBasicInfo = new OlderBasicInfo();
            olderBasicInfo.setOldPeopleId(oldPeople.getOldPeopleId());
            olderBasicInfo.setRelationship(oldPeople.getRelationship());
            olderBasicInfo.setAge(oldPeople.getAge());
            olderBasicInfo.setWeight(oldPeople.getWeight());
            olderBasicInfo.setHeight(oldPeople.getHeight());
            olderBasicInfo.setHobby(oldPeople.getHobby());
            list.add(olderBasicInfo);
        }
        return list;
    }
}




