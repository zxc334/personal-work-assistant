package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Article;
import com.example.personalworkassistant.domain.Category;
import com.example.personalworkassistant.service.ArticleService;
import com.example.personalworkassistant.service.CategoryService;
import com.example.personalworkassistant.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lzf
 * @version 1.0
 */
@Api(value = "文章模块相关接口")
@RestController
@RequestMapping("/api/article")
@Slf4j
@CrossOrigin
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserService userService;

    @ApiOperation(value = "上传文章")
    @PostMapping("/addArticle")
    public ResponseDTO addArticle(@RequestBody Article article) {
        Article article1 = articleService.addArticle(article);
        return ResponseDTO.success("添加成功", article1);
    }

    @ApiOperation(value = "搜索文章")
    @GetMapping("/searchArticle")
    public ResponseDTO searchArticle(@RequestParam("info") String info) {
        List<Article> articles = articleService.searchArticle(info);
        return ResponseDTO.success("搜索成功", articles);
    }

    @ApiOperation(value = "记录文章的观看量")
    @GetMapping("/addQuantity")
    public ResponseDTO addQuantity(@RequestParam("articleId") String articleId) {
        Article article = articleService.addQuantity(articleId);
        return ResponseDTO.success("记录成功", article);
    }
}
