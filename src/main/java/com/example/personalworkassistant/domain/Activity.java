package com.example.personalworkassistant.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
* 活动表
* @TableName activity
*/
@Data
public class Activity extends CommonBase implements Serializable {

    /**
    * 活动ID
    */
    @TableId
    private String id;
    /**
    * 活动名称
    */
    private String activityName;
    /**
    * cron执行表达式（定时任务）
    */
    private String cronExpression;
    /**
    * 计划执行级别（从1开始，逐级递减）
    */
    private Integer executionLevel;
    /**
    * 备注信息（如何进行运动，注意事项等）
    */
    private String remark;
    /**
     * 状态（0正常 1暂停）
     */
    private Integer status;
    /**
     * 活动时间
     */
    private String activeTime;
    /**
     * 提醒时间
     */
    private String remindTime;

}
