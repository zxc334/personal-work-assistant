package com.example.personalworkassistant.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
@Slf4j
@EnableKnife4j
@ComponentScan(basePackages = {"com.example.personalworkassistant"})//配置扫描的基础包
public class SwaggerConfig {

    @Value("${swagger2.enable}")
    private boolean swagger2Enable;

    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<String>(Arrays.asList("application/json", "application/xml"));

    @Bean
    public Docket docket1(Environment environment) {
        //选择显示的开发环境
        Profiles pro = Profiles.of("dev");
//        Profiles pro = Profiles.of("pro");
        //判断当前系统开发是否是开发环境
        boolean b = swagger2Enable;
        return new Docket(DocumentationType.SWAGGER_2)
                //文档基础信息
                .apiInfo(getApiInfo())
                //忽略参数类型
                .ignoredParameterTypes(HttpSession.class, HttpServletRequest.class, HttpServletResponse.class)
                //选择接口路径
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.personalworkassistant"))
                .build()
                //是否开启
                .enable(b)
                //分组名
                .groupName("家庭小助手");

    }

    private ApiInfo getApiInfo() {
        //联系人信息
        Contact contact =
                new Contact("lzf",
                        "",
                        "2712495982@qq.com");
        //Swagger文档基础配置
        ApiInfo info =
                new ApiInfo(
                        "家庭小助手管理文档",
                        "家庭小助手管理相关接口文档",
                        "v1.0",
                        "cust.edu.cn",
                        contact,
                        "Apache2.0",
                        "",
                        new ArrayList<>()
                );
        return info;
    }

}
