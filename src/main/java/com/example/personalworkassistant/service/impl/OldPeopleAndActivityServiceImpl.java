package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.OldPeopleAndActivity;
import com.example.personalworkassistant.dto.OlderActivityInfo;
import com.example.personalworkassistant.mapper.ActivityMapper;
import com.example.personalworkassistant.service.OldPeopleAndActivityService;
import com.example.personalworkassistant.mapper.OldPeopleAndActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lenovo
 * @description 针对表【old_people_and_activity】的数据库操作Service实现
 * @createDate 2024-01-19 18:11:39
 */
@Service
public class OldPeopleAndActivityServiceImpl implements OldPeopleAndActivityService {
    @Autowired
    private OldPeopleAndActivityMapper oldPeopleAndActivityMapper;
    @Autowired
    private ActivityMapper activityMapper;

    /**
     * 根据老人ID查询活动具体信息
     *
     * @param olderId
     * @return
     */
    @Override
    public List<OlderActivityInfo> queryActivityInfo(String olderId) {
        LambdaQueryWrapper<OldPeopleAndActivity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OldPeopleAndActivity::getOldPeopleId, olderId);
        wrapper.orderByDesc(OldPeopleAndActivity::getCreateTime); //按照创建时间排序
        List<OldPeopleAndActivity> oldPeopleAndActivityList = oldPeopleAndActivityMapper.selectList(wrapper);
        List<OlderActivityInfo> results = new ArrayList<>();
        for (OldPeopleAndActivity oldPeopleAndActivity : oldPeopleAndActivityList) {
            OlderActivityInfo olderActivityInfo = new OlderActivityInfo();
            Activity activity = activityMapper.selectById(oldPeopleAndActivity.getActivityId());
            olderActivityInfo.setActivityName(activity.getActivityName());
            olderActivityInfo.setRemark(activity.getRemark());
            olderActivityInfo.setOldPeopleId(oldPeopleAndActivity.getOldPeopleId());
            olderActivityInfo.setActiveTime(activity.getActiveTime());
            olderActivityInfo.setTodayExerciseTime(oldPeopleAndActivity.getTodayExerciseTime());
            olderActivityInfo.setIsCompleted(oldPeopleAndActivity.getIsCompleted());
            results.add(olderActivityInfo);
        }
        return results;
    }
}




