package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 对话表
 *
 * @TableName dialog
 */
@Data
public class Dialog {
    /**
     * ID
     */
    @TableId
    private String id;
    /**
     * 用户信息
     */
    private String userContent;
    /**
     * 机器信息
     */
    private String botContent;
    /**
     * 发送时间
     */
    private Long time;

}
