package com.example.personalworkassistant.dto;

import lombok.Data;

@Data
public class DialogDto {

    /**
     * 用户信息
     */
    private String userContent;
    /**
     * 机器信息
     */
    private String botContent;
}
