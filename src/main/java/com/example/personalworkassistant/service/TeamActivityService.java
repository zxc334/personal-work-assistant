package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.TeamActivity;
import com.example.personalworkassistant.dto.TeamActivityDto;

import java.util.List;

public interface TeamActivityService {
    /**
     * 创建团建活动
     * @param teamActivityDto
     */
    void createTeamActivity(TeamActivityDto teamActivityDto);

    /**
     * 查询所有团建活动
     */
    List<TeamActivity> queryTeamActivity();
}
