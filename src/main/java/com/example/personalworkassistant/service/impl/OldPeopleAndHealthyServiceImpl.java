package com.example.personalworkassistant.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.personalworkassistant.domain.OldPeopleAndHealthy;
import com.example.personalworkassistant.dto.OlderPhysicalCondition;
import com.example.personalworkassistant.service.OldPeopleAndHealthyService;
import com.example.personalworkassistant.mapper.OldPeopleAndHealthyMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Lenovo
 * @description 针对表【old_people_and_healthy】的数据库操作Service实现
 * @createDate 2024-01-19 18:11:39
 */
@Service
public class OldPeopleAndHealthyServiceImpl implements OldPeopleAndHealthyService {

    @Autowired
    private OldPeopleAndHealthyMapper oldPeopleAndHealthyMapper;

    /**
     * 根据老人ID获取身体情况信息
     *
     * @param olderId
     * @return
     */
    @Override
    public List<OlderPhysicalCondition> queryOlderHealthy(String olderId) {
        LambdaQueryWrapper<OldPeopleAndHealthy> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OldPeopleAndHealthy::getOldPeopleId, olderId);
        wrapper.orderByDesc(OldPeopleAndHealthy::getCreateTime);
        List<OldPeopleAndHealthy> oldPeopleAndHealthies = oldPeopleAndHealthyMapper.selectList(wrapper);
        List<OlderPhysicalCondition> list = new ArrayList<>();
        for (OldPeopleAndHealthy oldPeopleAndHealthy : oldPeopleAndHealthies) {
            OlderPhysicalCondition condition = new OlderPhysicalCondition();
            String info = null;
            //血压
            if (oldPeopleAndHealthy.getBloodPressureHigher() > 130) {
                info = info + "高压偏高，";
            } else if (oldPeopleAndHealthy.getBloodPressureLower() > 90) {
                info = info + "低压偏高，";
            } else {
                info = info + "血压正常，";
            }
            //血糖
            if (oldPeopleAndHealthy.getBloodGlucose() < 3.92) {
                info = info + "血糖偏低，";
            } else if (oldPeopleAndHealthy.getBloodGlucose() > 6.16) {
                info = info + "血糖偏高，";
            } else {
                info = info + "血糖正常，";
            }
            //心跳
            if (oldPeopleAndHealthy.getHeartbeat() < 55) {
                info = info + "心跳偏低，";
            } else if (oldPeopleAndHealthy.getBloodGlucose() > 80) {
                info = info + "心跳偏高，";
            } else {
                info = info + "心跳正常，";
            }
            //睡眠
            if (oldPeopleAndHealthy.getSleepingTime() < 5) {
                info = info + "睡眠不足";
            } else if (oldPeopleAndHealthy.getBloodGlucose() > 8) {
                info = info + "睡眠过多";
            } else {
                info = info + "睡眠正常";
            }
            condition.setInfo(info.substring(4));
            condition.setTime(oldPeopleAndHealthy.getCreateTime().toString().replace('T', ' '));
            BeanUtils.copyProperties(oldPeopleAndHealthy, condition);
            list.add(condition);
        }
        return list;
    }
}




