package com.example.personalworkassistant.dto;

import lombok.Data;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class TaskDto {
    /**
     * 任务名称
     */
    private String id;
    /**
     * 任务名称
     */
    private String taskName;



    /**
     * 计划执行级别（从1开始，逐级递减）
     */
    private String method;
}
