package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.Article;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.personalworkassistant.dto.AllInfoDto;
import com.example.personalworkassistant.dto.ExerciseDto;
import com.example.personalworkassistant.dto.RandomTaskDto;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【children_and_activity】的数据库操作Service
* @createDate 2024-01-19 18:11:39
*/
public interface ChildrenAndActivityService{
    /**
     * 根据用户ID获取对应的任务
     * @param userId
     */
    List<ChildrenAndActivity> queryTask(String userId);

    /**
     * 添加任务
     * @param childrenId
     */
    ChildrenAndActivity addTask(String childrenId,String activeId);

    /**
     * 添加任务
     * @param childrenId
     */
    int[][] queryHomework(String childrenId);

    List<ExerciseDto> queryExercise(String childrenId);
    int[][] queryAll(String childrenId);

    List<RandomTaskDto> randomDisplay(String childrenId);

    int randomNum(String childrenId);
    List<RandomTaskDto> randomRecord(String childrenId);

    Activity  randomUpdate(String activityId);
}
