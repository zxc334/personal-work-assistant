package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName children_and_recommend
 */
@TableName(value ="children_and_recommend")
@Data
public class ChildrenAndRecommend extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 孩子ID
     */
    private String childrenId;

    /**
     * 推荐ID
     */
    private String recommendId;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
