package com.example.personalworkassistant.dto;

import lombok.Data;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class AllInfoDto {
    private String name;
    private int data;
}
