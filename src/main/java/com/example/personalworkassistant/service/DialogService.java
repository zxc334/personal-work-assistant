package com.example.personalworkassistant.service;

import com.example.personalworkassistant.domain.Dialog;
import com.example.personalworkassistant.dto.DialogDto;

import java.util.List;

public interface DialogService {
    /**
     * 向对话表添加一条记录
     *
     * @param dialogDto 对话消息
     */
    void createMsg(DialogDto dialogDto);

    /**
     * 查询对话信息
     */
    List<Dialog> queryMsg();
}
