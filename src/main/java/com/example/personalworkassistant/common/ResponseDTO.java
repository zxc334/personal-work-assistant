package com.example.personalworkassistant.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Component;

/**
 * @author jww
 * @version 1.0
 * @description: Result前后端分离的数据结果集
 * @date 2022/6/20 14:18
 */
@ApiModel("返回值封装类")
@Component
public class ResponseDTO {
    @ApiModelProperty("状态码")
    private int status;
    @ApiModelProperty("提示信息")
    private String msg;
    @ApiModelProperty("返回值")
    private Object data;

    /**
     * 200-Success 400-Not Found(查询失败) 300-Wrong(写入失败)
     */
    public ResponseDTO() {
        this.status = 300;
        this.msg = "Wrong";
        this.data = null;
    }

    /**
     * 为统一异常处理器使用的构造方法，传参为300-Wrong-null
     */
    public ResponseDTO(int status, String msg, Object value) {
        this.status = status;
        this.msg = msg;
        this.data = value;
    }

    public static ResponseDTO success(String msg, Object value) {
        return new ResponseDTO(100, msg, value);
    }

    public static ResponseDTO fail(String msg, String value) {
        return new ResponseDTO(300, msg, value);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getValue() {
        return data;
    }

    public void setValue(Object value) {
        this.data = value;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                ", value=" + data +
                '}';
    }
}
