package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

/**
 * @author 李志峰
 * @version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/common")
public class CommonController {
    @Value("${family.path}")
    private String basePath;

    @ApiOperation(value = "上传图片")
    @PostMapping("/upload")
    public ResponseDTO upload(MultipartFile file){
        log.info(file.toString());
        String originalFilename = file.getOriginalFilename();
        String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID().toString()+substring;
        File dir = new File(basePath);
        if (!dir.exists()){
            dir.mkdirs();
        }
        try {
            file.transferTo(new File(basePath+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseDTO.success("上传成功",fileName);
    }
//blob:http://101.43.199.68:8160/a5b9e7cc-d387-4ad8-81a2-b6c277c62759
//
//    @ApiOperation(value = "下载图片")
//    @GetMapping("/download")
//    public void downLoad2(String name, HttpServletResponse response){
//        try {
//            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));
//            System.out.println(basePath + name);
//            FileOutputStream fileOutputStream = new FileOutputStream(new File("D:\\408"));
//            ServletOutputStream outputStream = response.getOutputStream();
//            response.setContentType("image/jpeg");
//            int len=0;
//            byte[] bytes = new byte[1024];
//            while ((len=fileInputStream.read(bytes))!=-1){
//                fileOutputStream.write(bytes);
//                outputStream.write(bytes);
//                outputStream.flush();
//            }
//            fileInputStream.close();
//            outputStream.close();
//            fileInputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
    @ApiOperation(value = "下载图片")
    @GetMapping("/download")
    public ResponseDTO downLoad(String name){
        try {
            File file = new File(basePath + name);
            byte[] byteArray = Files.readAllBytes(file.toPath());
            String base64String = Base64.getEncoder().encodeToString(byteArray);
            return ResponseDTO.success("请求成功",base64String);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
