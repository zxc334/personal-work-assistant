package com.example.personalworkassistant.dto;

import lombok.Data;

@Data
public class OlderPhysicalCondition {

    /**
     * 老人ID
     */
    private String oldPeopleId;

    /**
     * 高压值
     */
    private Integer bloodPressureHigher;

    /**
     * 低压值
     */
    private Integer bloodPressureLower;


    /**
     * 血糖值
     */
    private Double bloodGlucose;

    /**
     * 饭后血糖值
     */
    private Double bloodGlucoseAftereat;


    /**
     * 心跳值
     */
    private Integer heartbeat;

    /**
     * 睡眠时间
     */
    private Integer sleepingTime;

    /**
     * 测量时间
     */
    private String time;

    /**
     * 健康情况
     */
    private String info;
}
