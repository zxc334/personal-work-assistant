package com.example.personalworkassistant.mapper;

import com.example.personalworkassistant.domain.ChildrenAndRecommend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Lenovo
* @description 针对表【children_and_recommend】的数据库操作Mapper
* @createDate 2024-01-19 18:11:39
* @Entity com.example.personalworkassistant.domain.ChildrenAndRecommend
*/
@Mapper
public interface ChildrenAndRecommendMapper extends BaseMapper<ChildrenAndRecommend> {

}




