package com.example.personalworkassistant.dto;

import lombok.Data;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class Info {
    private String name;
    private Integer[] data;

}
