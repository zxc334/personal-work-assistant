package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Photo;
import com.example.personalworkassistant.service.PhotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "回忆录相关接口")
@RestController
@RequestMapping("/api/photo")
@Slf4j
@CrossOrigin
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @ApiOperation(value = "获取所有图片的网络地址")
    @GetMapping("/queryPhotoAddress")
    public ResponseDTO queryPhotoAddress() {
        List<Photo> list = photoService.queryPhotoAddress();
        return ResponseDTO.success("获取成功", list);
    }

    @ApiOperation(value = "插入图片的网络地址")
    @GetMapping("/insertPhoto")
    public ResponseDTO insertPhoto(@RequestParam("photoAddressArr") List<String> photoAddressArr) {
        photoService.insertPhoto(photoAddressArr);
        return ResponseDTO.success("插入成功", null);
    }
}
