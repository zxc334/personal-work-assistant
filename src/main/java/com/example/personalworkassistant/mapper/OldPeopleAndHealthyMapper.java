package com.example.personalworkassistant.mapper;

import com.example.personalworkassistant.domain.OldPeopleAndHealthy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Lenovo
* @description 针对表【old_people_and_healthy】的数据库操作Mapper
* @createDate 2024-01-19 18:11:39
* @Entity com.example.personalworkassistant.domain.OldPeopleAndHealthy
*/
@Mapper
public interface OldPeopleAndHealthyMapper extends BaseMapper<OldPeopleAndHealthy> {

}




