package com.example.personalworkassistant.service.impl;

import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.example.personalworkassistant.domain.OldPeopleAndActivity;
import com.example.personalworkassistant.dto.CreateActivity;
import com.example.personalworkassistant.dto.Task;
import com.example.personalworkassistant.dto.TaskDto;
import com.example.personalworkassistant.mapper.ActivityMapper;
import com.example.personalworkassistant.mapper.OldPeopleAndActivityMapper;
import com.example.personalworkassistant.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Lenovo
 * @description 针对表【activity(定时任务调度表)】的数据库操作Service实现
 * @createDate 2024-01-19 18:11:38
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private OldPeopleAndActivityMapper oldPeopleAndActivityMapper;

    /**
     * 创建活动
     *
     * @param createActivity
     */
    @Override
    public String createActivity(CreateActivity createActivity, String olderId) {
        //新建活动
        Activity activity = new Activity();
        activity.setActivityName(createActivity.getActivityName());
        activity.setRemark(createActivity.getRemark());
        activity.setCronExpression(createActivity.getMethod());
//        //将字符串转换为LocalDateTime类型
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDateTime remindTime = LocalDateTime.parse(createActivity.getRemindTime(), formatter);

        activity.setRemindTime(createActivity.getRemindTime());
        activity.setActiveTime(createActivity.getActiveTime());
        activity.setStatus(0);
        activityMapper.insert(activity);
        //将活动与老人关联
        if (olderId!=null){
            OldPeopleAndActivity opaa = new OldPeopleAndActivity();
            opaa.setActivityId(activity.getId());
            opaa.setOldPeopleId(olderId);
            oldPeopleAndActivityMapper.insert(opaa);
        }
        return activity.getId();
    }
    @Override
    public String createActivity(TaskDto task, String olderId) {
        System.out.println("传递值====》》"+task);
        //新建活动
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("M月d日 EEEE", Locale.CHINA);
        String formattedDate = dateFormat.format(currentDate);
        System.out.println("当前日期：" + formattedDate);

        Activity activity = new Activity();
        activity.setActivityName(task.getTaskName());
        activity.setRemark("未完成");
        activity.setCronExpression(task.getMethod());
        //将字符串转换为LocalDateTime类型
        activity.setActiveTime(formattedDate);
        activity.setStatus(0);
        activityMapper.insert(activity);
        //将活动与老人关联
        if (olderId!=null){
            OldPeopleAndActivity opaa = new OldPeopleAndActivity();
            opaa.setActivityId(activity.getId());
            opaa.setOldPeopleId(olderId);
            oldPeopleAndActivityMapper.insert(opaa);
        }
        return activity.getId();
    }

    @Override
    public Activity quaryActivity(String id) {
        Activity activity = activityMapper.selectById(id);
        return activity;
    }

    @Override
    public List<Task> quaryAllActivity(List<ChildrenAndActivity> childrenAndActivityList) {
        List<Task> tasks = new ArrayList<>();



        for (ChildrenAndActivity childrenAndActivity : childrenAndActivityList) {
            Activity activity = activityMapper.selectById(childrenAndActivity.getActivityId());
            System.out.println("activity::::"+activity);

            if (activity!=null){
                String adviceTime = activity.getActiveTime();
                String[] s = adviceTime.split(" ");
                Task task = new Task();
                task.setId(activity.getId());
                task.setTaskName(activity.getActivityName());
                task.setRemark(activity.getRemark());
                task.setMethod(activity.getCronExpression());
                task.setDate(s[0]);
                task.setWeek(s[1]);
                tasks.add(task);
            }
        }
        return tasks;
    }


}




