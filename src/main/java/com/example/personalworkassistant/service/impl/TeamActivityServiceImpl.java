package com.example.personalworkassistant.service.impl;

import com.example.personalworkassistant.domain.TeamActivity;
import com.example.personalworkassistant.dto.TeamActivityDto;
import com.example.personalworkassistant.mapper.TeamActivityMapper;
import com.example.personalworkassistant.service.TeamActivityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamActivityServiceImpl implements TeamActivityService {

    @Autowired
    private TeamActivityMapper teamActivityMapper;

    /**
     * 创建团建活动
     * @param teamActivityDto
     */
    @Override
    public void createTeamActivity(TeamActivityDto teamActivityDto) {
        TeamActivity teamActivity = new TeamActivity();
        BeanUtils.copyProperties(teamActivityDto,teamActivity);
        teamActivityMapper.insert(teamActivity);
    }

    /**
     * 查询所有团建活动
     */
    @Override
    public List<TeamActivity> queryTeamActivity() {
        List<TeamActivity> teamActivities = teamActivityMapper.selectList(null);
        return teamActivities;
    }
}
