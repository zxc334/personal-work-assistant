package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.domain.Category;
import com.example.personalworkassistant.service.CategoryService;
import com.example.personalworkassistant.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Lenovo
* @description 针对表【category】的数据库操作Service实现
* @createDate 2024-01-19 18:11:39
*/
@Service
public class CategoryServiceImpl  implements CategoryService{

    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Category> queryAllCategory() {
        List<Category> categories = categoryMapper.selectList(null);
        return categories;
    }

    @Override
    public Category addCategory(Category category) {
        Category category1 = new Category();
        category1.setName(category.getName());
        category1.setSort(category.getSort());
        category1.setIcon(category.getIcon());
        categoryMapper.insert(category1);
        return category1;
    }

    @Override
    public void deleteCategory(String categoryId) {
        categoryMapper.deleteById(categoryId);
    }

    @Override
    public Category updateCategory(Category category) {
        categoryMapper.updateById(category);
        return category;
    }
}




