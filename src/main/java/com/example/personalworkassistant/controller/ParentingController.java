package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.example.personalworkassistant.service.ActivityService;
import com.example.personalworkassistant.service.ArticleService;
import com.example.personalworkassistant.service.ChildrenAndActivityService;
import com.example.personalworkassistant.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lzf
 * @version 1.0
 */
@Api(value = "文章模块相关接口")
@RestController
@RequestMapping("/api/parenting")
@Slf4j
@CrossOrigin
public class ParentingController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ChildrenAndActivityService childrenAndActivityService;


    @ApiOperation(value = "根据用户ID查询任务安排")
    @GetMapping("/queryTask")
    public ResponseDTO queryTask(@RequestParam("userId") String userId ) {
        List<ChildrenAndActivity> childrenAndActivities = childrenAndActivityService.queryTask(userId);
        List<Activity> activities = new ArrayList<>();
        for (ChildrenAndActivity childrenAndActivity : childrenAndActivities) {
            Activity activity = activityService.quaryActivity(childrenAndActivity.getActivityId());
            activities.add(activity);
        }
        return ResponseDTO.success("查询成功", activities);
    }
}
