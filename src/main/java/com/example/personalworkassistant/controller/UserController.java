package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.dto.UserDto;
import com.example.personalworkassistant.service.ChildrenAndActivityService;
import com.example.personalworkassistant.service.FeedbackService;
import com.example.personalworkassistant.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author lzf
 * @version 1.0
 */
@Api(value = "用户模块相关接口")
@RestController
@RequestMapping("/api/user")
@Slf4j
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ChildrenAndActivityService childrenAndActivityService;
    @Autowired
    private FeedbackService feedbackService;
    /**
     * 用户注册接口
     */
    @ApiOperation(value = "用户注册接口")
    @PostMapping("/register")
    public ResponseDTO register(@RequestBody UserDto dto) {
        return userService.register(dto);
    }

    /**
     * 用户登录接口
     */
    @ApiOperation(value = "用户登录接口")
    @PostMapping("/login")
    public ResponseDTO login(@RequestBody UserDto dto) {
        return userService.login(dto);
    }
    /**
     * 用户反馈接口
     */
    @ApiOperation(value = "用户反馈接口")
    @PostMapping("/feedback")
    public ResponseDTO feedback(@RequestBody String feedback) {
        return feedbackService.feedback(feedback);
    }
}
