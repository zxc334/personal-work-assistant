package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName children_and_activity
 */
@TableName(value ="children_and_activity")
@Data
public class ChildrenAndActivity extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 孩子ID
     */
    private String childrenId;

    /**
     * 今日锻炼时间（分钟）
     */
    private String todayExerciseTime;

    /**
     * 今日活动完成度（锻炼时间/总时间）
     */
    private Double todayCompleteDegree;

    /**
     * 提醒时间
     */
    private Date remindTime;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
