package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 * @TableName old_people_and_activity
 */
@TableName(value = "old_people_and_activity")
@Data
public class OldPeopleAndActivity extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 活动id
     */
    private String activityId;

    /**
     * 老人ID
     */
    private String oldPeopleId;

    /**
     * 今日锻炼时间（分钟）
     */
    private String todayExerciseTime;

    /**
     * 是否完成
     */
    private Integer isCompleted;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
