package com.example.personalworkassistant.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.example.personalworkassistant.common.CommonBase;
import lombok.Data;

/**
 *
 * @TableName recommend
 */
@TableName(value ="recommend")
@Data
public class Recommend extends CommonBase implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 推荐食物（药品）名称
     */
    private String recommendName;

    /**
     * 用法说明（药品使用说明。忌口等）
     */
    private String statement;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
