package com.example.personalworkassistant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.personalworkassistant.domain.Photo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface PhotoMapper extends BaseMapper<Photo> {

    @Update("truncate table photo")
    void deleteAll();

}
