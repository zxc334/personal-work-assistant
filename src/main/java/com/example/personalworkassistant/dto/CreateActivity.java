package com.example.personalworkassistant.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateActivity implements Serializable {
    private String activityName;
    private String method;
    private String remark;
    private String remindTime;
    private String activeTime; //活动时间时间
}
