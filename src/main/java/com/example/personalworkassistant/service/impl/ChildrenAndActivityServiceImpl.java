package com.example.personalworkassistant.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.personalworkassistant.domain.Activity;
import com.example.personalworkassistant.domain.ChildrenAndActivity;
import com.example.personalworkassistant.dto.AllInfoDto;
import com.example.personalworkassistant.dto.ExerciseDto;
import com.example.personalworkassistant.dto.RandomTaskDto;
import com.example.personalworkassistant.mapper.ActivityMapper;
import com.example.personalworkassistant.mapper.CategoryMapper;
import com.example.personalworkassistant.service.ChildrenAndActivityService;
import com.example.personalworkassistant.mapper.ChildrenAndActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* @author Lenovo
* @description 针对表【children_and_activity】的数据库操作Service实现
* @createDate 2024-01-19 18:11:39
*/
@Service
public class ChildrenAndActivityServiceImpl implements ChildrenAndActivityService{
    @Autowired
    private ChildrenAndActivityMapper childrenAndActivityMapper;
    @Autowired
    private ActivityMapper activityMapper;
    @Override
    public List<ChildrenAndActivity> queryTask(String userId) {
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,userId);
        List<ChildrenAndActivity> childrenAndActivities = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
        return childrenAndActivities;
    }

    @Override
    public ChildrenAndActivity addTask(String childrenId, String activeId) {
        ChildrenAndActivity childrenAndActivity = new ChildrenAndActivity();
        childrenAndActivity.setActivityId(activeId);
        childrenAndActivity.setChildrenId(childrenId);
        childrenAndActivityMapper.insert(childrenAndActivity);
        return childrenAndActivity;
    }

    @Override
    public int[][] queryHomework(String childrenId) {
        Calendar cal=Calendar.getInstance();
        int y=cal.get(Calendar.YEAR);
        int mo=cal.get(Calendar.MONTH)+1;
        int d=cal.get(Calendar.DATE);
        int h=cal.get(Calendar.HOUR_OF_DAY);
        int mi=cal.get(Calendar.MINUTE);
        int s=cal.get(Calendar.SECOND);
        System.out.println("现在时刻是"+y+"年"+mo+"月"+d+"日"+h+"时"+mi+"分"+s+"秒");
        int[][] homeWork=new int[3][7];
        List<ExerciseDto> exerciseDtos = new ArrayList<>();
        for (int i = 0; i < homeWork.length; i++) {
            for (int j = 0; j < homeWork[0].length; j++) {
                homeWork[i][j]=0;
            }
        }
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,childrenId);
        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
        for (ChildrenAndActivity childrenAndActivity : list) {
            String activityId = childrenAndActivity.getActivityId();
            Activity activity = activityMapper.selectById(activityId);
            if (activity!=null){
                String remark = activity.getRemark();
                String activityName = activity.getActivityName();
                if (remark!=null&&activityName!=null){
                    if (activity.getActivityName().equals("作业")&&activity.getRemark().equals("已完成")){
                        System.out.println("ac:::"+activity);
                        String activeTime = activity.getActiveTime();
                        String[] split = activeTime.split(" ");
                        String data=split[0];
                        Pattern pattern = Pattern.compile("\\d+");
                        Matcher matcher = pattern.matcher(data);
                        int index=0;
                        int[] array={0,0};
                        while (matcher.find()){
                            array[index]=Integer.parseInt(matcher.group());
                            index++;
                        }
                        String week=split[1];
                        int now=mo*30+d;
                        int act=array[0]*30+array[1];
                        if (act<=now){
                            int i=0;
                            if (now-act<=7){
                                i=1;
                            }
                            if (week.equals("星期一")){
                                homeWork[i][0]++;
                            }else if (week.equals("星期二")){
                                homeWork[i][1]++;
                            }else if (week.equals("星期三")){
                                homeWork[i][2]++;
                            }else if (week.equals("星期四")){
                                homeWork[i][3]++;
                            }else if (week.equals("星期五")){
                                homeWork[i][4]++;
                            }else if (week.equals("星期六")){
                                homeWork[i][5]++;
                            }else if (week.equals("星期日")){
                                homeWork[i][6]++;
                            }
                        }
                    }

                }


            }
        }
        return homeWork;
    }

    @Override
    public List<ExerciseDto> queryExercise(String childrenId) {
        List<ExerciseDto> exerciseDtos = new ArrayList<>();
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,childrenId);
        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
        for (ChildrenAndActivity childrenAndActivity : list) {
            String activityId = childrenAndActivity.getActivityId();
            Activity activity = activityMapper.selectById(activityId);
            if (activity!=null){
                String remark = activity.getRemark();
                String activityName = activity.getActivityName();
                if (remark!=null&&activityName!=null){
                    if (activity.getRemark().equals("已完成")&&!activity.getActivityName().equals("作业")){
                        String activeTime = activity.getActiveTime();
                        String[] split = activeTime.split(" ");
                        String data=split[0];
                        Pattern pattern = Pattern.compile("\\d+");
                        Matcher matcher = pattern.matcher(data);
                        int index=0;
                        int[] array={0,0};
                        while (matcher.find()){
                            array[index]=Integer.parseInt(matcher.group());
                            index++;
                        }
                        String week=split[1];
                        System.out.println("pppppp");

                        if (exerciseDtos.isEmpty()){
                            System.out.println("isEmpty");
                            ExerciseDto exerciseDto1 = new ExerciseDto();
                            exerciseDto1.setName(activity.getActivityName());
                            if (week.equals("星期一")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[0]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期二")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[1]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期三")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[2]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期四")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[3]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期五")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[4]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期六")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[5]++;
                                exerciseDto1.setData(data1);
                            } else if (week.equals("星期日")) {
                                int[] data1 = exerciseDto1.getData();
                                data1[6]++;
                                exerciseDto1.setData(data1);
                            }
                            exerciseDtos.add(exerciseDto1);
                            System.out.println("exerciseDtos..."+exerciseDtos);
                        }else{
                            boolean found = false;
                            for (int indx=0;indx<exerciseDtos.size();indx++) {
                                System.out.println("indx:::"+indx);
                                ExerciseDto exerciseDto = exerciseDtos.get(indx);
                                System.out.println("ttt");
                                if (exerciseDto.getName().equals(activity.getActivityName())){
                                    found = true;
                                    System.out.println("ccccc"+exerciseDtos);

                                    int act=array[0]*30+array[1];
                                    if (week.equals("星期一")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[0]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期二")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[1]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期三")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[2]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期四")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[3]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期五")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[4]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期六")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[5]++;
                                        exerciseDto.setData(dtoData);
                                    } else if (week.equals("星期日")) {
                                        int[] dtoData = exerciseDto.getData();
                                        dtoData[6]++;
                                        exerciseDto.setData(dtoData);
                                    }
                                }
                            }
                            if (!found){
                                System.out.println("iiii");
                                ExerciseDto exerciseDto1 = new ExerciseDto();
                                exerciseDto1.setName(activity.getActivityName());
                                if (week.equals("星期一")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[0]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期二")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[1]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期三")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[2]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期四")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[3]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期五")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[4]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期六")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[5]++;
                                    exerciseDto1.setData(data1);
                                } else if (week.equals("星期日")) {
                                    int[] data1 = exerciseDto1.getData();
                                    data1[6]++;
                                    exerciseDto1.setData(data1);
                                }
                                exerciseDtos.add(exerciseDto1);
                            }
                        }

                    }
                }


            }
        }
        exerciseDtos.sort(Comparator.comparingInt(ExerciseDto::getSumOfData).reversed());

        // 保留前三个对象，删除其余对象
        while (exerciseDtos.size() > 3) {
            exerciseDtos.remove(exerciseDtos.size() - 1);
        }
        return exerciseDtos;
    }

    @Override
    public int[][] queryAll(String childrenId) {
        int[][] res=new int[2][4];
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,childrenId);
        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
        for (ChildrenAndActivity childrenAndActivity : list) {
            String activityId = childrenAndActivity.getActivityId();
            Activity activity = activityMapper.selectById(activityId);
            if (activity!=null) {
                String remark = activity.getRemark();
                String activityName = activity.getActivityName();
                String activeTime = activity.getActiveTime();
                String[] split = activeTime.split(" ");
                String data = split[0];
                Pattern pattern = Pattern.compile("\\d+");
                Matcher matcher = pattern.matcher(data);
                int index = 0;
                int[] array = {0, 0};
                while (matcher.find()) {
                    array[index] = Integer.parseInt(matcher.group());
                    index++;
                }
                if (remark.equals("已完成") && activityName.equals("作业")) {
                    if (array[1] <= 8) {
                        res[0][0]++;
                    } else if (array[1] <= 16) {
                        res[0][1]++;
                    } else if (array[1] <= 24) {
                        res[0][2]++;
                    } else if (array[1] <= 32) {
                        res[0][3]++;
                    }
                }
                if(remark.equals("已完成") && !activityName.equals("作业")){
                    if (array[1] <= 8) {
                        res[1][0]++;
                    } else if (array[1] <= 16) {
                        res[1][1]++;
                    } else if (array[1] <= 24) {
                        res[1][2]++;
                    } else if (array[1] <= 32) {
                        res[1][3]++;
                    }
                }
            }
        }

        return res;
    }

    @Override
    public List<RandomTaskDto> randomDisplay(String childrenId) {
        ArrayList<RandomTaskDto> randomTaskDtos = new ArrayList<>();
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId, childrenId);

        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);

        List<RandomTaskDto> allRandomTasks = new ArrayList<>();

        for (ChildrenAndActivity childrenAndActivity : list) {
            String activityId = childrenAndActivity.getActivityId();
            Activity activity = activityMapper.selectById(activityId);

            if (activity != null && activity.getCronExpression().equals("随机选")&&activity.getStatus()==0) {
                RandomTaskDto randomTaskDto = new RandomTaskDto();
                randomTaskDto.setId(activityId);
                randomTaskDto.setTaskName(activity.getActivityName());
                randomTaskDto.setMethod(activity.getCronExpression());
                allRandomTasks.add(randomTaskDto);
            }
        }

        // 随机排序所有符合条件的数据
        Collections.shuffle(allRandomTasks);

        // 取前8条数据返回
        List<RandomTaskDto> resultList = allRandomTasks.subList(0, Math.min(8, allRandomTasks.size()));

        return resultList;
    }

    @Override
    public int randomNum(String childrenId) {
        List<RandomTaskDto> randomTaskDtos = randomDisplay(childrenId);
        Random random = new Random();
        int num=random.nextInt(8) + 1;
        for (RandomTaskDto randomTaskDto : randomTaskDtos) {
            if (randomTaskDto.getNum()==num){
                Activity activity = activityMapper.selectById(randomTaskDto.getId());
                activity.setStatus(1);
                activityMapper.updateById(activity);
            }
        }
        return num;
    }

    @Override
    public List<RandomTaskDto> randomRecord(String childrenId) {
        ArrayList<RandomTaskDto> randomTaskDtos = new ArrayList<>();
        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,childrenId);
        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
        for (ChildrenAndActivity childrenAndActivity : list) {
            String activityId = childrenAndActivity.getActivityId();
            Activity activity = activityMapper.selectById(activityId);
            if (activity!=null) {
                if (activity.getStatus()==1){
                    RandomTaskDto randomTaskDto = new RandomTaskDto();
                    randomTaskDto.setId(activityId);
                    randomTaskDto.setTaskName(activity.getActivityName());
                    randomTaskDto.setMethod(activity.getCronExpression());
                    randomTaskDtos.add(randomTaskDto);
                }
            }
        }
        return randomTaskDtos;
    }

    @Override
    public Activity randomUpdate(String activityId) {
        Activity activity = activityMapper.selectById(activityId);
        activity.setStatus(1);
        activityMapper.updateById(activity);
        return null;
    }


//    @Override
//    public int[][] queryExercise(String childrenId) {
//        ArrayList<ExerciseDto> exerciseDtos = new ArrayList<>();
//        LambdaQueryWrapper<ChildrenAndActivity> childrenAndActivityLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        childrenAndActivityLambdaQueryWrapper.eq(ChildrenAndActivity::getChildrenId,childrenId);
//        List<ChildrenAndActivity> list = childrenAndActivityMapper.selectList(childrenAndActivityLambdaQueryWrapper);
//        for (ChildrenAndActivity childrenAndActivity : list) {
//            String activityId = childrenAndActivity.getActivityId();
//            Activity activity = activityMapper.selectById(activityId);
//            if (activity!=null){
//                if (activity.getActivityName()!="作业"&&activity.getRemark().equals("已完成")){
//                    for (ExerciseDto exerciseDto : exerciseDtos) {
//                        if (exerciseDto.getName().equals(activity.getActivityName())){
//
//                        }
//                    }
//                }
//            }
//        }
//        return new int[0][];
//    }

}




