package com.example.personalworkassistant.dto;

import lombok.Data;

/**
 * @author lzf
 * @version 1.0
 */
@Data
public class Task {
    /**
     * id名称
     */
    private String id;
    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 备注信息（如何进行运动，注意事项等）
     */
    private String remark;

    /**
     * 计划执行级别（从1开始，逐级递减）
     */
    private String method;
    /**
     * 日期
     */
    private String date;
    /**
     * 星期
     */
    private String week;
}
