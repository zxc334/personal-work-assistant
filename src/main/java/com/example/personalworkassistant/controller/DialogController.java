package com.example.personalworkassistant.controller;

import com.example.personalworkassistant.common.ResponseDTO;
import com.example.personalworkassistant.domain.Dialog;
import com.example.personalworkassistant.dto.DialogDto;
import com.example.personalworkassistant.service.DialogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "对话框相关接口")
@RestController
@RequestMapping("/api/dialog")
@Slf4j
@CrossOrigin
public class DialogController {

    @Autowired
    private DialogService dialogService;

    @ApiOperation(value = "添加一条对话并显示所有对话")
    @PostMapping("/info")
    public ResponseDTO queryOlderHealthy(@RequestBody DialogDto dialogDto) {
        //1.向数据库中加入一条信息
        dialogService.createMsg(dialogDto);
        //2.返回所有的对话信息
        List<Dialog> dialogs = dialogService.queryMsg();
        return ResponseDTO.success("获取成功", dialogs);
    }

    @ApiOperation(value = "初始化")
    @GetMapping("/init")
    public ResponseDTO init() {
        List<Dialog> dialogs = dialogService.queryMsg();
        return ResponseDTO.success("获取成功", dialogs);
    }

}
